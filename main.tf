resource "random_string" "rand" {
    length = 5
    special = false
    upper = false
    
}

resource "google_storage_bucket" "buckets" {
    count = 3
    name = "${var.name}-${(count.index)+1}-${random_string.rand.result}"
    storage_class = var.storage_class
    location = var.region
}